import React, { Component } from 'react';

class GoogleMap extends Component {

  // lifecycle method that gets called once the component has been rendered to the screen
  componentDidMount() {
    // This is generally now we integrate 3rd party JS libraries that don't know how to
    // work with a react environment. The first argument to the Map library is the
    // render location in the DOM (usually an id reference). Here instead of using an id reference
    // we are giving it the reference in Javascript instead.
    // The second arguemnt is an option object used to tell the Maps library how to render the map.
    new google.maps.Map(this.refs.map, {
      zoom: 12,
      center: {
        lat: this.props.lat, // this is usually a number but we're gonna use the lat and long from the props
        lng: this.props.lon
      }
    });
  }

  render() {
    // makes use of the ref system in react (reference)
    // ref system allows us to get a reference to an HTML element that's been rendered to a page
    // by referring to this.refs.map
    return <div ref="map" />
  }

}

export default GoogleMap;
