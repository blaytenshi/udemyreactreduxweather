// axios is used to make ajax requests and returns them in a JS promise object
import axios from 'axios';

const API_KEY = 'd494489ca12f47674a997ca07b4fa59f';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

// Action Creator responsible for making AJAX request for Weather Data
export function fetchWeather(city) {

  const url = `${ROOT_URL}&q=${city},us`;
  // returns a promise here
  const request = axios.get(url);

  return {
    type: FETCH_WEATHER, // the correct way to create a type with a const
    // returning the promise as the payload... or is it?
    // ReduxPromise stops the action, waits for the request to resolve
    // once it returns, it actually strips out the data and puts it in the payload
    payload: request
  };
}
