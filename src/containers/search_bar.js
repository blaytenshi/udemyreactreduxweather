import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchWeather } from '../actions/index';

class SearchBar extends Component {

  constructor(props) {
    super(props);

    this.state = { term: '' };
    this.onInputChange = this.onInputChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onInputChange(event) {
    console.log(event.target.value);
    this.setState({ term: event.target.value });
  }

  // this is the function that'll get called when the submit button is clicked
  // in the form. As you can see, it prevents the default action from being fired.
  onFormSubmit(event) {
    event.preventDefault();

    // after preventing the default, we can instruct the app to go fetch data
    // from an API from this point.
    this.props.fetchWeather(this.state.term); // making the ajax call through axios with fetchweather action creator
    // added to reset the input field of the search term
    this.setState({ term: ''});
  }

  render() {
    return(
      // normally when you click submit on an input that is in a form-control
      // it will try to submit the form to a given url. Since no URL is given
      // the app will just submit back onto itself. We want to prevent this
      // by telling the form to call the onFormSubmit() when the submit button
      // is clicked.

      // The reason for using form is two folds, when it's all in a form element
      // we get a bunch of default handling for free such as when the user
      // clicks the submit button as well as when they hit the enter button
      // we don't need to create new event listeners to listen for certain situations
      // eg only submit when there's stuff in the input, submit when the button is clicked
      // OR when enter key is pressed, etc.
      <form onSubmit={this.onFormSubmit} className="input-group">
        <input
          placeholder="Get a five-day forecast in your favourite cities"
          className="form-control"
          value={this.state.term}
          // if you have a callback that makes a reference to 'this', chances are you need to bind it
          // you usually use a fat arrow function (that takes the context of the outside this) like so:
          // { () => this.onInputChange() }
          // but doing it in the constructor is another way
          onChange={this.onInputChange}
        />
        <span className="input-group-btn">
          <button type="submit" className="btn btn-secondary">Submit</button>
        </span>
      </form>
    )
  };
}

// binds action creators from the import to the application so when an action creator
// is called, the reducers get them from the dispatcher
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchWeather }, dispatch);
}

// the null is to say we're not passing anything to the application state
export default connect(null, mapDispatchToProps)(SearchBar);
