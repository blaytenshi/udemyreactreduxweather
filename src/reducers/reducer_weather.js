import { FETCH_WEATHER } from '../actions/index';

export default function(state = [], action) {
  switch (action.type) {
    // imported from the action
    case FETCH_WEATHER:
      // because we need to add this piece of data into the state array we may be tempted to do
      // return state.push([action.payload.data]);
      // but this is a subtle trap! Remember you should be returning new states not mutating them!
      // so we use can concat instead
      // return state.concat([action.payload.data]);
      // OR we can use the ES6 syntax below which inserts each action.payload.data as an
      // entry into the state object like [action.payload.data, action.payload.data, ...]
      // the one thing to note with using this method of inserting is that
      // this will put the last inserted piece of data at the very top
      return [ action.payload.data, ...state ];
  }
  return state;
}
